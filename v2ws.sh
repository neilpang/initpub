





_uuid="$1"

_wspath="${2:-phpmyadmin123}"
_hostdomain="${3:-*.287360.xyz}"

_certname="${4:-287360}"




if [ -z "$_uuid" ]; then
  echo "Usage:  uuid  [phpmyadmin123]  [sub.287360.xyz]  [certname]" 
  exit 1
fi




_v2ws="$HOME/sites/v2ws"

if [ "$2" ]; then
  _v2ws="$HOME/sites/$2"
fi


mkdir -p "$_v2ws"


docker pull v2fly/v2fly-core:latest



cat >"$_v2ws/config.json" <<'END_CAT'

{
  "log": {
    "loglevel": "info"
  },
  "inbound": {
    "port": 10001,
    "protocol": "vmess",
    "allocate": {
      "strategy": "always"
    },
    "settings": {
      "clients": [
        {
          "id": "_uuid_",
          "level": 1,
          "alterId": 0,
          "security": "auto"
        }
      ]
    },
    "streamSettings": {
      "network": "ws",
      "wsSettings": {
        "path": "/phpmyadmin123/"
      }
    }
  },
  "outbound": {
    "protocol": "freedom"
  }
}

END_CAT


sed -i "s/_uuid_/$_uuid/g" "$_v2ws/config.json"


cat >"$_v2ws/run.sh" <<'END_CAT'
#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit


vdomain=_hostdomain_

certname=_certname_

container_name="v2ws"
wspath="/_wspath_/"

myvhost=~/proxy/vhost.d
myfolder=$myvhost/my


_port=" -p 10001:10001"
_forward="\$host"


if [ "$wspath" != "/phpmyadmin123/" ]; then
  _port=" -p 10002:10001"
  container_name="_wspath_"
  _forward=$container_name
fi





function start() {
  useacme="$1"


  mkdir -p $myfolder

  echo "
location ${wspath} {
        proxy_redirect off;
        proxy_pass http://${_forward}:10001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \"upgrade\";
        proxy_set_header Host \$http_host;

        # Show realip in v2ray access.log
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
}


">$myfolder/${container_name}.conf


  echo "return 444;" >$myvhost/${vdomain}_location


  sed -i "s#/phpmyadmin123/#${wspath}#"  config.json

  _cfg="$(pwd)/config.json"
  if [ -e "$_cfg.local" ]; then
    _cfg="$_cfg.local"
  fi

  if [ "$certname" ] && [ -e "$HOME/proxy/certs/$certname.crt" ]; then
    docker run -d --name ${container_name} \
    --restart=unless-stopped \
    -e VIRTUAL_HOST=$vdomain \
    -e CERT_NAME=$certname \
    -e ENABLE_SOCKS=true \
    -e HTTPS_METHOD=noredirect  $_port \
    -v /etc/hosts:/etc/hosts:ro \
    -v $_cfg:/etc/v2ray/config.json \
    v2fly/v2fly-core run -c /etc/v2ray/config.json 

  elif [ -e "$HOME/proxy/certs/default.crt" ] && ! [ -e "$HOME/proxy/certs/$vdomain.crt" ] && [ -z "$useacme" ]; then
    docker run -d --name ${container_name} \
    --restart=unless-stopped \
    -e VIRTUAL_HOST=$vdomain \
    -e ENABLE_SOCKS=true \
    -e HTTPS_METHOD=noredirect $_port \
    -v /etc/hosts:/etc/hosts:ro \
    -v $_cfg:/etc/v2ray/config.json \
    v2fly/v2fly-core run -c /etc/v2ray/config.json 
  
  else

    docker run -d --name ${container_name} \
    --restart=unless-stopped \
    -e VIRTUAL_HOST=$vdomain \
    -e ENABLE_ACME=true \
    -e SSL_POLICY=Mozilla-Modern \
    -e HSTS=off \
    -e ENABLE_SOCKS=true \
    -e HTTPS_METHOD=noredirect $_port \
    -v /etc/hosts:/etc/hosts:ro \
    -v $_cfg:/etc/v2ray/config.json \
    v2fly/v2fly-core run -c /etc/v2ray/config.json 

  fi








}


function stop() {
    rm -f $myfolder/${container_name}.conf
    rm -f $myvhost/${vdomain}_location
    docker rm -f ${container_name}

}


"$@"






END_CAT




sed -i "s/_hostdomain_/$_hostdomain/g" "$_v2ws/run.sh"

sed -i "s/_certname_/$_certname/g" "$_v2ws/run.sh"

sed -i "s/_wspath_/$_wspath/g" "$_v2ws/run.sh"

chmod +x "$_v2ws/run.sh"


(
cd "$_v2ws"
./run.sh  stop
./run.sh  start "useacme"


)
 
