

apt-get install fail2ban




cat >/etc/fail2ban/jail.d/sshd.local <<END_EOF


[DEFAULT]
ignoreip = 156.251.180.185 45.92.77.6 45.92.77.202 193.32.150.124




[sshd]
port    = 10086
logpath = %(sshd_log)s
backend = %(sshd_backend)s



[sshd-10087]
port    = 10087
logpath = %(sshd_log)s
backend = %(sshd_backend)s






END_EOF


service fail2ban restart


service fail2ban status

sleep 1

fail2ban-client status sshd



