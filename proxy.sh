#!/bin/bash


set -e



DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
mkdir -p $DOCKER_CONFIG/cli-plugins
curl -SL https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-$(uname -s)-$(uname -m) -o $DOCKER_CONFIG/cli-plugins/docker-compose
chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose

docker compose

(

cd "$HOME"
mkdir -p proxy

cd proxy


mkdir -p stream.d
cd stream.d

curl https://init.287360.xyz/proxy/mul.conf > mul.conf
curl https://init.287360.xyz/proxy/mul.js > mul.js


curl https://init.287360.xyz/proxy/alpn.30443.conf > alpn.30443.conf


cd ..



curl https://init.287360.xyz/proxy/run.sh > run.sh



rm -f nginx.tmpl

chmod +x run.sh





./run.sh stop
./run.sh start
 







)