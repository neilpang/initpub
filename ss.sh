





_uuid="$1"




if [ -z "$_uuid" ]; then
  echo "Usage:  uuid " 
  exit 1
fi




_ss="$HOME/sites/ss"

mkdir -p "$_ss"



cat >"$_ss/run.sh" <<'END_CAT'
#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit


container_name=ss


function start() {

    docker run -d --name ${container_name} \
    --restart=unless-stopped \
    -p 20315:8388 \
    -p 20315:8388/udp \
    -e PASSWORD=_uuid_ \
    shadowsocks/shadowsocks-libev


}


function stop() {

    docker rm -f ${container_name}

}


"$@"






END_CAT


sed -i "s/_uuid_/$_uuid/g" "$_ss/run.sh"

chmod +x "$_ss/run.sh"


(
cd "$_ss"
./run.sh  stop
./run.sh  start 


)
 
