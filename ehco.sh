




_home="$HOME/sites/ehco"

mkdir -p "$_home"



cat >"$_home/config.json" <<'END_CAT'

{

    "relay_configs": [

        
        {
            "listen": "0.0.0.0:21001",
            "listen_type": "wss",
            "transport_type": "raw",
            "tcp_remotes": [
                "127.0.0.1:10443"
            ]
        }

    ]
}

END_CAT



if [ ! -e "$_home/ehco" ]; then
  if uname -a | grep "aarch64"; then
    wget https://github.com/Ehco1996/ehco/releases/download/v1.1.1/ehco_1.1.1_linux_arm64  -O $_home/ehco
  else
    wget https://github.com/Ehco1996/ehco/releases/download/v1.1.1/ehco_1.1.1_linux_amd64  -O $_home/ehco
  fi

  chmod +x $_home/ehco

fi





cat >"$_home/run.sh" <<'END_CAT'
#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit



container_name="ehco"





function start() {


  docker run -d -it --name ${container_name} \
  --restart=unless-stopped \
  --net=host \
  -v $(pwd):/app \
  ubuntu /app/ehco -c /app/config.json



}


function stop() {

    docker rm -f ${container_name}

}


"$@"






END_CAT



chmod +x "$_home/run.sh"



(

cd $_home
./run.sh  start

)