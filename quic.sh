





_uuid="$1"
_key="$2"  #not used for now



if [ -z "$_uuid" ]; then
  echo "Usage:  uuid  key"
  exit 1
fi




_quic="$HOME/sites/quic"

mkdir -p "$_quic"

docker pull v2fly/v2fly-core:latest



cat >"$_quic/config.json" <<'END_CAT'

{
  "log": {
    "loglevel": "info"
  },
  "inbound": {
    "port": 20001,
    "protocol": "vmess",
    "allocate": {
      "strategy": "always"
    },
    "settings": {
      "clients": [
        {
          "id": "_uuid_",
          "level": 1,
          "alterId": 0,
          "security": "auto"
        }
      ]
    },
    "streamSettings": {
      "network": "quic",
      "quicSettings": {
        "security": "none",
        "key": "_key_",
        "header": {
          "type": "none"
        }
      }
    }
  },
  "outbound": {
    "protocol": "freedom"
  }
}


END_CAT


sed -i "s/_uuid_/$_uuid/g" "$_quic/config.json"
#sed -i "s|_key_|$_key|g" "$_quic/config.json"


cat >"$_quic/run.sh" <<'END_CAT'
#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit


function start() {

  _cfg="$(pwd)/config.json"
  if [ -e "$_cfg.local" ]; then
    _cfg="$_cfg.local"
  fi

  docker run -d --name quic \
  --restart=unless-stopped \
  -v /etc/hosts:/etc/hosts:ro \
  -v $_cfg:/etc/v2ray/config.json \
  -p 10443:20001/udp \
  v2fly/v2fly-core run -c /etc/v2ray/config.json 

}


function stop() {
    docker rm -f quic

}


"$@"





END_CAT



chmod +x "$_quic/run.sh"


(
cd "$_quic"
./run.sh  stop
./run.sh  start


)
 
