

_v2ws="$HOME/sites/sslh"


mkdir -p "$_v2ws"





cat >"$_v2ws/run.sh" <<'END_CAT'
#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit


start() {

docker run \
  --name=sslh \
  -itd --net=host \
  --restart=unless-stopped \
  neilpang/sslh:latest  -n \
  --listen=0.0.0.0:10443 \
  --http=127.0.0.1:80 \
  --tls=127.0.0.1:443 \
  --ssh=127.0.0.1:10086 \
  --anyprot=127.0.0.1:20001

}



stop() {
  docker rm -f sslh

}


"$@"






END_CAT



chmod +x "$_v2ws/run.sh"


(
cd "$_v2ws"
./run.sh  stop
./run.sh  start


)

