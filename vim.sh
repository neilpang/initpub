

apt-get update

apt-get install -y  vim curl

echo "
set encoding=utf-8
set fileencodings=utf-8,cp950
 
syntax on                                                                                                                                                                                                  
    
set nocompatible

set ruler        
set backspace=2  
set ic           
set ru           
set hlsearch     
set incsearch    

set confirm      
set history=100  
 
 
set laststatus=2
set statusline=%4*%<\%m%<[%f\%r%h%w]\ [%{&ff},%{&fileencoding},%Y]%=\[Position=%l,%v,%p%%]


">~/.vimrc



