





_uuid="$1"




if [ -z "$_uuid" ]; then
  echo "Usage:  uuid  "
  exit 1
fi




_v2ws="$HOME/sites/vmess"

mkdir -p "$_v2ws"

docker pull v2fly/v2fly-core:latest


cat >"$_v2ws/config.json" <<'END_CAT'

{
  "log": {
    "loglevel": "info"
  },
  "inbound": {
    "port": 20001,
    "protocol": "vmess",
    "allocate": {
      "strategy": "always"
    },
    "settings": {
      "clients": [
        {
          "id": "_uuid_",
          "level": 1,
          "alterId": 0,
          "security": "auto"
        }
      ]
    }
  },
  "outbound": {
    "protocol": "freedom"
  }
}

END_CAT


sed -i "s/_uuid_/$_uuid/g" "$_v2ws/config.json"


cat >"$_v2ws/run.sh" <<'END_CAT'
#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit


function start() {

  _cfg="$(pwd)/config.json"
  if [ -e "$_cfg.local" ]; then
    _cfg="$_cfg.local"
  fi

  docker run -d --name vmess \
  --restart=unless-stopped \
  -v /etc/hosts:/etc/hosts:ro \
  -v $_cfg:/etc/v2ray/config.json \
  -p 20001:20001 \
  v2fly/v2fly-core run -c /etc/v2ray/config.json 

}


function stop() {
    docker rm -f vmess

}


"$@"





END_CAT



chmod +x "$_v2ws/run.sh"


(
cd "$_v2ws"
./run.sh  stop
./run.sh  start


)
 
