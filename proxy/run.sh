#!/usr/bin/env bash


cd "$(
  cd "$(dirname "$0")" || exit
  pwd
)" || exit


function start() {

    mkdir -p vhost.d/my/
    echo '
include /etc/nginx/vhost.d/my/*;


'>vhost.d/default



    if [ ! -e "$(pwd)/certs/default.crt" ]; then
      mv $(pwd)/stream.d/alpn.30443.conf $(pwd)/stream.d/alpn.30443.conf.bak
    else
      mv $(pwd)/stream.d/alpn.30443.conf.bak $(pwd)/stream.d/alpn.30443.conf
    fi


    if [ -e "$(pwd)/nginx.tmpl.local" ]; then
      _tpl="-v $(pwd)/nginx.tmpl.local:/app/nginx.tmpl"
    fi


    docker run   -it  -d  \
        -v /var/run/docker.sock:/tmp/docker.sock:ro  \
        -v /etc/hosts:/etc/hosts:ro \
        -v $(pwd)/certs:/etc/nginx/certs \
        -v $(pwd)/acme:/acmecerts \
        -v $(pwd)/conf.d:/etc/nginx/conf.d \
        -v $(pwd)/vhost.d:/etc/nginx/vhost.d \
        -v $(pwd)/stream.d:/etc/nginx/stream.d \
        -v $(pwd)/dhparam:/etc/nginx/dhparam \
        -v $(pwd)/htpasswd:/etc/nginx/htpasswd  $_tpl \
        --name proxy \
        --net=host \
        --restart=unless-stopped \
        -e ENABLE_IPV6=true \
        neilpang/letsproxy:alpine


    docker run -d \
        -v /var/run/docker.sock:/tmp/docker.sock:ro \
        -v /etc/hosts:/tmp/hosts \
        --restart=unless-stopped \
        --name=hoster \
        neilpang/docker-hoster



#docker run -d \
#    --name watchtower \
#    -v /var/run/docker.sock:/var/run/docker.sock \
#    --restart=unless-stopped \
#    containrrr/watchtower --cleanup --interval 3600




}


function stop() {
    docker rm -f watchtower
    docker rm -f proxy
    docker rm -f hoster
}


function restart() {
    nohup "./run.sh stop && ./run.sh start" &
#    docker restart watchtower
    docker restart hoster
}


function reload() {

    docker exec -it proxy  nginx -s reload

}


"$@"

