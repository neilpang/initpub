
set -e

echo "swap size: ${1:-1024}"

free -m

if [ -e "/var/swap.img" ]; then
  swapoff /var/swap.img
fi

dd if=/dev/zero of=/var/swap.img bs=1024k count=${1:-1024}
chmod 600 /var/swap.img
mkswap /var/swap.img
chmod 600 /var/swap.img
swapon /var/swap.img


if ! grep /var/swap.img /etc/fstab; then
  echo '/var/swap.img   none    swap    sw    0   0' >>/etc/fstab
fi

free -m

